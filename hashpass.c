#include <stdio.h>
#include <stdlib.h>
#include <string.h>

char *md5(const char *str, int length);

int main(int argc, char *argv[])
{
    if (argc < 3)
    {
        printf("Please supply an input and an output file\n");
        exit(2);
    }
    
    FILE *fpin;
    fpin = fopen(argv[1], "r");
    if (!fpin)
    {
        printf("Can't open %s for reading\n", argv[1]);
        exit(1);
    }
    
    FILE *fpout;
    fpout = fopen(argv[2], "w");
    if (!fpout)
    {
        printf("Can't open %s for writing\n", argv[2]);
        exit(1);
    }
    
    char line[100];
    while(fgets(line, 100, fpin) != NULL)
    {
        if(line[strlen(line)-1] == '\n')
        {
            line[strlen(line)-1] = '\0';
        }
        fprintf(fpout, "%s\n", md5(line, strlen(line)));
    }
    fclose(fpin);
    fclose(fpout);
}